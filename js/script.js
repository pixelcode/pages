$('.middlediv-part, .verweis').click(function(){
	window.location = $(this).attr('href');
});

$('.navbutton, .inhaltsvz-eintrag-nav').click(function(){
	$('nav').toggleClass('nav-aktiv');
	$('.navbutton i').toggleClass('fa-bars');
	$('.navbutton i').toggleClass('fa-times');
});
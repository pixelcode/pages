$('.darkmode-span').click(function(){
	Cookies.set('darkmode','true');
	darken();
});

$('.lightmode-span').click(function(){
	if(cookiedark() != 'true'){
		Cookies.set('darkmode','false');
		lighten();
		location.reload();
		return false;
	}
	
	Cookies.set('darkmode','false');
	lighten();
});

function osdark(){
	return window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;
}

function cookiedark(){
	return  Cookies.get('darkmode');
}

if(cookiedark() == 'true'){
	darken();
}

if(cookiedark() == 'false'){
	lighten();
}

function darken(){
	$('.darkmode-span').css({ 'display': 'none' });
	$('.lightmode-span').css({ 'display': 'inline-block' });
	if(osdark() != 'true'){
		$('link[href="css/auto-dark.css"]').remove();
		addCSS('css/dark.css');
	}
}

function lighten(){
	$('link[href="css/dark.css"]').remove();
	$('link[href="css/auto-dark.css"]').remove();
	$('.darkmode-span').css({ 'display': 'inline-block' });
	$('.lightmode-span').css({ 'display': 'none' });
}

function addCSS(filename){
	var head = document.getElementsByTagName('head')[0];
	var style = document.createElement('link');
	style.href = filename;
	style.type = 'text/css';
	style.rel = 'stylesheet';
	head.append(style);
}

$('#encrypttoggle').click(function(){
	if($('#encrypttoggle').hasClass('togglenotselected')){
		$('.togglebutton').removeClass('toggleselected');
		$('.togglebutton').removeClass('togglenotselected');
		$('#encrypttoggle').addClass('toggleselected');
		$('#decrypttoggle').addClass('togglenotselected');
		if($('#weiter').css('display') == 'inline-block'){
			start();
		}
	}
	
	encryption();
});

$('#decrypttoggle').click(function(){
	if($('#decrypttoggle').hasClass('togglenotselected')){
		$('.togglebutton').removeClass('toggleselected');
		$('.togglebutton').removeClass('togglenotselected');
		$('#decrypttoggle').addClass('toggleselected');
		$('#encrypttoggle').addClass('togglenotselected');
		if($('#weiter').css('display') == 'inline-block'){
			start();
		}
	}
	
	decryption();
});

$('#plaintext').on('input propertychange paste', function(){
	$(this).addClass('angefasst');
	plainfehler();
});

$('#passwort').on('input propertychange paste', function(){
	$(this).addClass('angefasst');
	passfehler();
});


$('#passwort').keyup(function(){
	$(this).addClass('angefasst');
	passfehler();
});

$('#plaintext').keyup(function(){
	$(this).addClass('angefasst');
	plainfehler();
});

$('#passwort').focusin(function(){
	$('#passwortanzeigen-outer').addClass('pw-outer-light-bg');
});

$('#passwort').focusout(function(){
	$('#passwortanzeigen-outer').removeClass('pw-outer-light-bg');
});


$('#passwortanzeigen').click(function(){
	var x = document.getElementById("passwort");
	if (x.type === "password") {
		x.type = "text";
		$('#passwortanzeigen').html('<i class="far fa-eye"></i>');
	} else {
		x.type = "password";
		$('#passwortanzeigen').html('<i class="fas fa-eye-slash"></i>');
	}
});

$('#los').click(function(){
	$('#passwort, #plaintext').addClass('angefasst');
	if($('#encrypttoggle').hasClass('toggleselected')){
		passwort = $('#passwort').val();
		text = $('#plaintext').val();
		if(passwort.length >= 1 && text.length >= 1){
			ergebnis();
			var encrypted = CryptoJS.AES.encrypt(text, passwort);
			encrypted = encrypted.toString();
			$('#ergebnis').val(encrypted);
		} else {
			plainfehler();
			passfehler();
		}
	} else {
		passwort = $('#passwort').val();
		text = $('#plaintext').val();
		if(passwort.length >= 1 && text.length >= 1){
			ergebnis();
			var decrypted = CryptoJS.AES.decrypt(text, passwort);
			decrypted = decrypted.toString(CryptoJS.enc.Utf8);
			$('#ergebnis').val(decrypted);
		} else {
			plainfehler();
			passfehler();
		}
	}
});

function plainfehler(){
	if($('#plaintext').is(':invalid')){
		$('#plainfehler').css({ 'visibility': 'visible' });
	} else {
		$('#plainfehler').css({ 'visibility': 'hidden' });
	}
}

function passfehler(){
	if($('#passwort').is(':invalid')){
		$('#passfehler').css({ 'visibility': 'visible' });
		$('#passfehler').removeClass('warnung');
		$('#passwort').removeClass('warning');
		$('#passfehler').text('Please fill out this field');
	} else if($('#passwort').val().length >= 1 && $('#passwort').val().length <= 10){
		$('#passfehler').css({ 'visibility': 'visible' });
		$('#passfehler').addClass('warnung');
		$('#passwort').addClass('warning');
		$('#passfehler').text('Your password is very weak.');
	} else {
		$('#passfehler').css({ 'visibility': 'hidden' });
		$('#passfehler').removeClass('warnung');
		$('#passwort').removeClass('warning');
		$('#passfehler').text('Please fill out this field');
	}
	
	if($('#passwort').val().length >= 1){
		$('#passwortanzeigen').css({ 'display': 'inline' });
	} else {
		$('#passwortanzeigen').css({ 'display': 'none' });
	}
}

function start(){
	$('#eingabediv').css({ 'display': 'inline' });
	$('#plaintext').val('');
	$('#passwort').val('');
	$('#passwort, #plaintext').removeClass('angefasst');
	$('#passwort').removeClass('warning');
	$('#passfehler, #plainfehler').css({ 'visibility': 'hidden' });
	$('#passwortanzeigen').css({ 'display': 'none' });
	$('#ergebnisdiv').css({ 'display': 'none' });
	$('#los').css({ 'display': 'inline' });
	$('#weiter').css({ 'display': 'none' });
}

function decryption(){
	$('#eingabebeschreibung').text('Enter your ciphertext');
	$('#passwortbeschreibung').text('Enter your password');
	$('#los').text('Decrypt');
}

function encryption(){
	$('#eingabebeschreibung').html('Enter your plaintext');
	$('#passwortbeschreibung').text('Enter your password');
	$('#encryptbutton').css({ 'display': 'inline' });
	$('#decryptbutton').css({ 'display': 'none' });
	$('#los').text('Encrypt');
}

function ergebnis(){
	$('#eingabediv').css({ 'display': 'none' });
	$('#ergebnisdiv').css({ 'display': 'inline' });
	$('#los').css({ 'display': 'none' });
	$('#weiter').css({ 'display': 'inline-block' });
}

$('#weiter').click(function(){
	start();
});
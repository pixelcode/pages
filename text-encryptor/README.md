# Text Encryptor
This is a pure HTML, CSS and JS implementation of the Firefox addon [Text Encryptor](https://codeberg.org/pixelcode/TextEncryptor)

## Screenshot (German version)
![Screenshot](https://codeberg.org/pixelcode/OnlineTextEncryptor/raw/commit/df54458df90268f0d9411cbf1edaddc6a3a8027c/Screenshot.png)

## Usage
Download this repository by either clicking on the download button above **or** by git-cloning it:
```git clone https://codeberg.org/pixelcode/OnlineTextEncryptor```


#### Web server
Upload the ```OnlineTextEncryptor``` folder to your webserver. Your **Online Text Encryptor** instance will be available at ```http://example.com/OnlineTextEncryptor/```. If you don't like the folder's name just rename it and re-upload it to your server.


#### Local machine
Open the ```index.html``` file in a browser such as Firefox/Tor, Chromium or Opera. Most browsers will show you a working demo of your website thanks to the fact that **Online Text Encryptor** doesn't need any server-side code or backend.

## Documentation and security
Please refer to [the original repository](https://codeberg.org/pixelcode/TextEncryptor).

## Translation and i18n
If you want to use **Online Text Encryptor** in another language than German please refer to **Text Encryptor**'s [localization](https://codeberg.org/pixelcode/TextEncryptor/src/branch/master/_locales) and copy the text strings you need. Currently, only German and English are supported. Please consider contributing 😊

## License
**Text Encryptor** uses **[CrypoJS'](https://github.com/brix/crypto-js)** aes.js created by [Evan Vosberg (brix)](https://github.com/brix). It may be used according to the [MIT license](https://opensource.org/licenses/MIT). Thank you, Evan!

**Text Enryptor** uses **[jQuery](https://jquery.com)**.

**Text Encryptor** uses **[Font Awesome](https://fontawesome.com)** icons. These may be used according to the [Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/) license.

You may use **Text Encryptor**'s source code without explicit permission but a link to my repo would be great of course 😎
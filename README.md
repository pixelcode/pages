So this is just a little test playing around with Codeberg's new feature allowing us to present static HTML pages. Thanks, Codeberg 👍❤️

# Address
[pixelcode.codeberg.page](https://pixelcode.codeberg.page)

# Licence 
This repository is placed under the [For Good Eyes Only Licence v0.1](https://codeberg.org/pixelcode/pages/raw/branch/master/For%20Good%20Eyes%20Only%20Licence%20v0.1.pdf). Do not re-use the title "PhishWarn", the PhishWarn logo and the Pixelcode logo.